package com.ander.cursos.services;


import com.ander.commons.microservicio.service.Generic_Service;
import com.ander.cursos.models.entity.Curso;

public interface CursoService extends Generic_Service<Curso> {
   //31-1
	public Curso findCursoByAlumnId(Long id);
	
	//61. -> 3 Implementando cliente HTTP Feign en cursos para obteners examenes respondidos
	public Iterable<Long> obtenerExamenesIdsConRespuestasAlumno( Long alumnoId);
}
