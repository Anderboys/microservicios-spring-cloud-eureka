package com.ander.cursos.services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ander.commons.microservicio.service.Generic_ServiceImpl;
import com.ander.cursos.clients.RespuestaFeignClient;
import com.ander.cursos.models.entity.Curso;
import com.ander.cursos.repository.CursoRepository;

@Service
public class CursoServiceImpl extends Generic_ServiceImpl<Curso, CursoRepository> implements CursoService {

	@Autowired
	private RespuestaFeignClient client;
	
	//31-2
	@Override
	@Transactional(readOnly = true)
	public Curso findCursoByAlumnId(Long id) {
		return repository.findCursoByAlumnId(id);
	}

	//61. -> 4 Implementando cliente HTTP Feign en cursos para obteners examenes respondidos
	//  NO VA -> @Transactional , ya que no es 1 repository, no es una comunicacion con Base de datos
	@Override
	public Iterable<Long> obtenerExamenesIdsConRespuestasAlumno(Long alumnoId) {		
		return client.obtenerExamenesIdsConRespuestasAlumno(alumnoId);
	}


}
 