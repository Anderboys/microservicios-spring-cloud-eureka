package com.ander.cursos.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ander.commons.alumnos.models.entity.Alumno;
import com.ander.commons.examenes.models.entity.Examen;
import com.ander.commons.microservico.controller.Generic_Controller;
import com.ander.cursos.models.entity.Curso;
import com.ander.cursos.services.CursoService;

@RestController
public class CursoController extends Generic_Controller<Curso, CursoService>{
	
	
	//64 -> 1 . Probando Balanceo de carga Spring Cloud Load Balancer
	@Value("${config.balanceador.test}")
	//config.balanceador.test -> unicado en el application.properties
	private String balanceadorTest;
	
	@GetMapping("/balanceador-test")
	public ResponseEntity<?> balanceadortest() {
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("balanceador", balanceadorTest);
		response.put("cursos", service.findAll());
		return ResponseEntity.ok(response);
	}
	
	

	@PutMapping("/{id}")
	public ResponseEntity<?> editar(@Valid @RequestBody Curso curso,BindingResult result, @PathVariable Long id){
		
		// (validar) metodo protected, tanto para Editar como para Crear POST y PUT
		if(result.hasErrors()) {
			return this.validar(result);
		}
		
		Optional<Curso> o = this.service.findById(id);
		if(!o.isPresent()){
			return ResponseEntity.notFound().build();
		}
		Curso dbCurso = o.get();
		dbCurso.setNombre(curso.getNombre());
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(dbCurso));
	}


	// como es una lista -> List,  en el postam el formato json se envia con corchetes ejm: [{"id":"1"} ]
	@PutMapping("/{id}/asignar-alumnos")
	public ResponseEntity<?> asignarAlumnos(@RequestBody List<Alumno> alumnos, @PathVariable Long id){
		Optional<Curso> o = this.service.findById(id);
		if(!o.isPresent()){
			return ResponseEntity.notFound().build();
		}
		Curso dbCurso = o.get();
	   // List<Alumno> alumnos 
		alumnos.forEach(a -> {
			dbCurso.addAlumno(a);
		});
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(dbCurso));
	}
	
	@PutMapping("/{id}/eliminar-alumno")
	public ResponseEntity<?> eliminarAlumno(@RequestBody Alumno alumno, @PathVariable Long id){
		Optional<Curso> o = this.service.findById(id);
		if(!o.isPresent()){
			return ResponseEntity.notFound().build();
		}
		Curso dbCurso = o.get();
		dbCurso.removeAlumno(alumno);
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(dbCurso));
	}
	
	// 31-3
	@GetMapping("/alumno/{id}")
	public ResponseEntity<?> buscarAlumnoID(@PathVariable Long id){
		Curso curso = service.findCursoByAlumnId(id);		
		
		//62. Comunicacion con servicio respuesta para obtener ids de examenes respondidos
		// json:Get -> localhost:8090/api/cursos/alumno/2
		// "respondido": true  Or "respondido": false
		if(curso!= null) {
		  List<Long>examenesIds  = (List<Long>) service.obtenerExamenesIdsConRespuestasAlumno(id);
		  // utilizando api string de java 8 con flujos y programacion funcional
		  List<Examen> examenes = curso.getExamenes().stream().map(examen -> {
			  if(examenesIds.contains(examen.getId())) {
				  examen.setRespondido(true);
			  }
			  return examen;
		  }).collect(Collectors.toList());
		  curso.setExamenes(examenes);			
		}
		
		return ResponseEntity.ok(curso);
	}
	
	
	//38. -> 5 Añadiendo relación entre cursos y exámenes metodos -> asignar-examenes && eliminar-examen
	
	@PutMapping("/{id}/asignar-examenes")
	public ResponseEntity<?> asignarExamenes(@RequestBody List<Examen> examenes, @PathVariable Long id){
		Optional<Curso> o = this.service.findById(id);
		if(!o.isPresent()){
			return ResponseEntity.notFound().build();
		}
		Curso dbCurso = o.get();
	   // List<Alumno> alumnos 
		examenes.forEach(e -> {
			dbCurso.addExamenes(e);
		});
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(dbCurso));
	}
	
	
	@PutMapping("/{id}/eliminar-examen")
	public ResponseEntity<?> eliminarExamen(@RequestBody Examen examen, @PathVariable Long id){
		Optional<Curso> o = this.service.findById(id);
		if(!o.isPresent()){
			return ResponseEntity.notFound().build();
		}
		Curso dbCurso = o.get();
		dbCurso.removeExamene(examen);
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(dbCurso));
	}
	

	
}
